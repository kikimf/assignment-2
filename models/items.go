package models

type Items struct {
	ItemId      int    `json:"item_id" gorm:"primary_key;auto_increment;not_null"`
	ItemCode    string `json:"item_code"`
	Description string `json:"description"`
	Quantity    int    `json:"quantity"`
	OrderId     int    `gorm:"column:order_id" json:"order_id"`
}
