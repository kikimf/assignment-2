package models

type Orders struct {
	OrderId      int     `json:"order_id" gorm:"primary_key;auto_increment;not_null"`
	CustomerName string  `json:"customer_name"`
	OrderedAt    string  `json:"ordered_at"`
	Items        []Items `gorm:"ForeignKey:OrderId"`
}
