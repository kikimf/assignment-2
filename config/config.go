package config

import (
	"os"

	"github.com/joho/godotenv"
)

var (
	dbCon Database
	apps  Apps
)

type Database struct {
	Host, Username, Password, Port, DbName string
}

type Apps struct {
	Port string
}

func GetConDb() Database {
	godotenv.Load("config/.env")
	dbCon.Host = os.Getenv("HOST")
	dbCon.Username = os.Getenv("USERNAME")
	dbCon.Password = os.Getenv("PASSWORD")
	dbCon.DbName = os.Getenv("DBNAME")
	dbCon.Port = os.Getenv("PORT")
	return dbCon
}

func GetApps() Apps {
	godotenv.Load("config/.env")
	apps.Port = os.Getenv("PORTAPPS")
	return apps
}
