package database

import (
	"assignment2/config"
	"assignment2/models"
	"fmt"
	"log"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
)

var (
	dbConfig = config.GetConDb
	db       *gorm.DB
	err      error
)

func StartDB() *gorm.DB {
	config := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8&parseTime=True&loc=Local", dbConfig().Username, dbConfig().Password, dbConfig().Host, dbConfig().Port, dbConfig().DbName)
	db, err = gorm.Open("mysql", config)
	if err != nil {
		log.Fatal("error connection to database : ", err)
	}
	db.AutoMigrate(models.Orders{}, models.Items{})
	return db
}
