package middleware

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func BasicAuth(c *gin.Context) {
	user, password, hashAuth := c.Request.BasicAuth()
	if hashAuth && user == "risky" && password == "fajri" {
		c.Next()
	} else {
		c.Abort()
		c.Writer.Header().Set("WWW-Authenticate", "Basic realm=Restircted")
		c.JSON(401, gin.H{
			"message": http.StatusText(http.StatusUnauthorized),
		})
		return
	}
}
