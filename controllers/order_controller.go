package controllers

import (
	"assignment2/models"
	"assignment2/utils"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
)

type inDB struct {
	DB *gorm.DB
}

func NewInDB(db *gorm.DB) inDB {
	return inDB{
		DB: db,
	}
}

func (db *inDB) GetOrder(c *gin.Context) {

	var (
		order models.Orders
	)
	id := c.Param("id")
	err := db.DB.Preload("Items").Where("order_id = ?", id).First(&order).Error
	if err != nil {
		utils.SuccessResponse(c, 00, "data not found", nil)
	} else {
		utils.SuccessResponse(c, 00, "success get data", order)
	}
}

func (db *inDB) GetOrders(c *gin.Context) {

	var (
		orders []models.Orders
	)

	db.DB.Preload("Items").Find(&orders)
	if len(orders) <= 0 {
		utils.SuccessResponse(c, 00, "data not found", nil)
	} else {
		utils.SuccessResponse(c, 00, "success get data", orders)
	}

}

func (db *inDB) CreateOrder(c *gin.Context) {

	var (
		order models.Orders
	)
	if err := c.ShouldBindJSON(&order); err != nil {
		utils.FailResponse(c, 02, "error parsing data", err)
		return
	}

	db.DB.Create(&order)
	for _, item := range order.Items {
		item.OrderId = order.OrderId
		db.DB.Create(&item)
	}

	utils.SuccessResponse(c, 00, "success create data", order)
}

func (db *inDB) UpdateOrder(c *gin.Context) {
	id := c.Query("id")

	var (
		order    models.Orders
		newOrder models.Orders
	)

	if err := c.ShouldBindJSON(&newOrder); err != nil {
		utils.FailResponse(c, 02, "error parsing data", err)
		return
	}

	err := db.DB.First(&order, id).Error
	if err != nil || order.OrderId == 0 {
		utils.FailResponse(c, 02, "error data not found", err)
		return
	}

	err = db.DB.Model(&order).Updates(newOrder).Error

	if err != nil {
		utils.FailResponse(c, 02, "error update data", err)
		return
	} else {
		utils.SuccessResponse(c, 00, "success update data", newOrder)
		return
	}

}

func (db *inDB) DeleteOrder(c *gin.Context) {
	id := c.Param("id")

	var (
		order models.Orders
	)

	err := db.DB.First(&order, id).Error

	if err != nil || order.OrderId == 0 {
		utils.FailResponse(c, 02, "error data not found", err)
		return
	}

	err = db.DB.Where("order_id = ?", id).Delete(&models.Items{}).Error

	err2 := db.DB.Delete(&models.Orders{}, id).Error

	if err != nil || err2 != nil {
		utils.FailResponse(c, 02, "error delete data", err)
		return
	} else {
		utils.SuccessResponse(c, 00, "success delete data", nil)
		return
	}

}

func (db *inDB) UpdateItem(c *gin.Context) {
	id := c.Query("id")

	var (
		item    models.Items
		newItem models.Items
	)

	if err := c.ShouldBindJSON(&newItem); err != nil {
		utils.FailResponse(c, 02, "error parsing data", err)
		return
	}

	err := db.DB.First(&item, id).Error
	if err != nil || item.ItemId == 0 {
		utils.FailResponse(c, 02, "error data not found", err)
		return
	}

	newItem.OrderId = item.OrderId

	err = db.DB.Model(&item).Updates(newItem).Error

	if err != nil {
		utils.FailResponse(c, 02, "error update data", err)
		return
	} else {
		utils.SuccessResponse(c, 00, "success update data", newItem)
		return
	}

}
