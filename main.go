package main

import (
	"assignment2/config"
	"assignment2/config/database"
	"assignment2/controllers"

	"github.com/gin-gonic/gin"
)

func main() {
	db := database.StartDB()
	inDB := controllers.NewInDB(db)
	apps := config.GetApps()
	router := gin.Default()
	router.GET("/order/:id", inDB.GetOrder)
	router.GET("/orders", inDB.GetOrders)
	router.POST("/order", inDB.CreateOrder)
	router.PUT("/order", inDB.UpdateOrder)
	router.PUT("/item", inDB.UpdateItem)
	router.DELETE("/order/:id", inDB.DeleteOrder)
	router.Run(apps.Port)
}
