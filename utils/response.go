package utils

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func SuccessResponse(c *gin.Context, status int, message string, data interface{}) {
	var result = gin.H{
		"code":    status,
		"message": message,
		"data":    data,
	}
	c.JSON(http.StatusOK, result)
}

func FailResponse(c *gin.Context, status int, message string, err error) {
	var result = gin.H{
		"code":    status,
		"message": message,
		"error":   err.Error(),
	}
	c.JSON(http.StatusOK, result)
}
