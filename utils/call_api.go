package utils

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

func CallApi(url string) ([]byte, error) {
	res, err := http.Get(url)

	if err != nil {
		log.Fatalln(err)
		return nil, err
	}

	fmt.Println(res.Body)

	body, err := ioutil.ReadAll(res.Body)

	if err != nil {
		log.Fatalln(err)
		return nil, err
	}
	defer res.Body.Close()

	return body, nil
}
